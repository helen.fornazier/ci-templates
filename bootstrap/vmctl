#!/bin/bash
#
# CHANGES TO THIS SCRIPT REQUIRE BUMPING OF THE qemu_tag.

PORT=5555

die() {
    echo "$@" >&2
    exit 1
}

usage() {
    echo "Usage: vmctl {start|start-kernel|stop|exec} [command]"
    echo ""
    echo "Options: "
    echo "  start ... start the virtual machine"
    echo "  start-kernel ... start the virtual machine with a custom kernel"
    echo "  stop  ... stop the virtual machine"
    echo "  exec  ... exec command on the virtual machine"
}

find_ssh_config_dir() {
    # To allow a user to run this script,
    if [[ -z "$SSH_CONFIG_DIR" ]]; then
        ssh_config_dir=/etc/ssh/ssh_config.d/
        if [[ "$(whoami)" != "root" ]]; then
            ssh_config_dir="$HOME/.ssh/config.d"
            if ! [[ -d $ssh_config_dir ]]; then
                die "$ssh_config_dir does not exist and \$SSH_CONFIG_DIR is unset"
            fi
        fi
        SSH_CONFIG_DIR="$ssh_config_dir"
    fi
}

do_start() {
    set -ex

    if [[ ! -e /app/image.raw ]]
    then
      xz -d -T0 /app/image.raw.xz || die "Failed to unpack image"
    fi

    find_ssh_config_dir

    set +e
    outdir="$PWD"
    if [[ -n "$CI_PROJECT_DIR" ]] ; then
        outdir=$(realpath "$CI_PROJECT_DIR")
    fi

    qemu-system-x86_64 -machine accel=kvm \
                       -smp 2 -m 1024 \
                       -drive format=raw,file=/app/image.raw \
                       -device virtio-net-pci,netdev=net0 \
                       -netdev user,id=net0,hostfwd=tcp::$PORT-:22 \
                       -display none \
                       "$@" \
                       -serial file:$outdir/console.out &
    qemu_pid=$!
    # If we survive the first second there was no immediate permission or argument error
    sleep 1
    if ! kill -0 $qemu_pid; then
        wait $qemu_pid
        exit_code=$?
    fi

    echo "$qemu_pid" > qemu.pid

    if [[ ! -e $SSH_CONFIG_DIR/99-vm.conf ]]; then
        mkdir -p $SSH_CONFIG_DIR
        cat >$SSH_CONFIG_DIR/99-vm.conf <<EOF
            ControlMaster auto
            ControlPersist 5

            Host vm
              HostName localhost
              User root
              Port $PORT
              PreferredAuthentications publickey
              IdentitiesOnly yes
              IdentityFile %d/.ssh/ci-templates-vm
EOF
    fi

    # Connect once to store the host key locally
    if [[ $exit_code -eq 0 ]]; then
        ssh -vvv $VMCTL_SSH_OPTS -o ControlMaster=no -o StrictHostKeyChecking=accept-new vm uname -a
        exit_code=$?

        # Sometimes we get disconnected despite everything being ok. Let's
        # try again, but this time without verbosity
        if [[ $exit_code -ne 0 ]]; then
                echo "SSH connect failed, retrying..."
                retries=2
                while [[ $exit_code -ne 0 ]] && [[ $retries -gt 0 ]]; do
                        sleep 10
                        ssh $VMCTL_SSH_OPTS -o StrictHostKeyChecking=accept-new vm uname -a
                        exit_code=$?
                        retries=$(($retries - 1))
                done
        fi
    fi

    if [[ $exit_code -ne 0 ]]; then
        cat <<EOF
***********************************************************
*                                                         *
*       WARNING: failed to start or connect to VM         *
*          Serial console output is below                 *
*                                                         *
***********************************************************
EOF
       cat $outdir/console.out
       cat <<EOF
***********************************************************"
*                                                         *"
*       WARNING: failed to start or connect to VM         *"
*                                                         *"
***********************************************************"
EOF
       do_stop 1
    fi

    exit 0
}

do_start_kernel() {
    set -x

    KERNEL=$1
    shift

    set -e

    # default to the latest kernel
    if [[ x"$KERNEL" == x"" ]]
    then
        KERNEL=$(ls /app/vmlinuz* | sort | tail -1)
    fi

    INITRD=$(ls /app/initr* | sort | tail -1)

    do_start -kernel $KERNEL \
             -initrd $INITRD \
             -append "root=/dev/sda2 selinux=0 audit=0 rw console=tty0 console=ttyS0" \
             "$@"
}

do_stop() {
    exitcode=$1
    set -x

    do_exec halt -p
    slept=2
    sleep 2

    if [[ -e qemu.pid ]]; then
        qemu_pid=$(cat qemu.pid)
        rm qemu.pid
    else
        qemu_pid=$(pgrep qemu)
    fi

    while kill -0 "$qemu_pid" 2> /dev/null; do
        slept=$((slept + 2))
        if [[ $slept -gt 30 ]]; then
            kill -TERM "$qemu_pid"
            break;
        fi
        sleep 2
    done

    find_ssh_config_dir
    rm -f "$SSH_CONFIG_DIR"/99-vm.conf

    exit $exitcode
}

do_exec() {
    set -x
    ssh $VMCTL_SSH_OPTS vm "$@"
}

reset_known_hosts() {
    # Remove any known hosts entries for this machine
    if [[ -n "$HOME" ]]; then
        sed -i "/^\[localhost\]:$PORT/d" $HOME/.ssh/known_hosts
    fi
}

case $1 in
    start)
        shift
        do_start "$@"
        ;;
    start-kernel)
        shift
        do_start_kernel "$@"
        ;;
    stop)
        shift
        do_stop "$@"
        ;;
    exec)
        shift
        do_exec "$@"
        ;;
    reset-known-hosts)
        reset_known_hosts "$@"
        shift
        ;;
    *)
        usage
        exit 1
        ;;
esac
